-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

-- Your code here
local composer = require("composer")
composer.gotoScene("SceneSplash", {effect = "fade", time = 1000});

display.setStatusBar(display.HiddenStatusBar)