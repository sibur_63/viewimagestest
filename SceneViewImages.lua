local composer = require( "composer" )
local scene = composer.newScene()
 
local widget = require( "widget" )
local json = require( "json" )
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 -- размера экрана и координаты центра экрана
 local screenWidth = display.contentWidth
 local screenHeight = display.contentHeight
 local centerX = display.contentCenterX
 local centerY = display.contentCenterY
 
 local scrollView
 local linkImage
 local counterImage = 1
 local counterSizeHeight = 0
 
 -- ScrollView listener
local function scrollListener( event )
 
    local phase = event.phase
    if ( phase == "began" ) then print( "Scroll view was touched" )
    elseif ( phase == "moved" ) then print( "Scroll view was moved" )
    elseif ( phase == "ended" ) then print( "Scroll view was released" )
    end
 
    -- In the event a scroll limit is reached...
    if ( event.limitReached ) then
        if ( event.direction == "up" ) then print( "Reached bottom limit" )
        elseif ( event.direction == "down" ) then print( "Reached top limit" )
        elseif ( event.direction == "left" ) then print( "Reached right limit" )
        elseif ( event.direction == "right" ) then print( "Reached left limit" )
        end
    end
 
    return true
end

-- извлечение данных из файла
local function loadInfo(filename)
	-- получаем путь к файлу
	local path = system.pathForFile(filename, system.ResourceDirectory);
	local contents = "";
	local myTable = {};
	local file = io.open(path, "r"); -- открываем файл
	if (file) then -- если такой файл существует
		 local contents = file:read( "*a" ); -- читаем из него данные
		 myTable = json.decode(contents); -- расшифровываем их
		 io.close(file); -- закрываем файл
		 return myTable; -- возвращаем параметры из файла
	end
	return nil
end

-- слушатель загрузки изображений (обрабатывает изображения и вставляет в scrollView)
local function networkListener( event )
    if ( event.isError ) then
        print( "Network error - download failed: ", event.response )
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        print( "Displaying response image file" )
        myImage = display.newImage( event.response.filename, event.response.baseDirectory)
		myImage.x = centerX
		print(myImage.contentHeight)
		myImage.y = counterSizeHeight + myImage.contentHeight / 2
		counterSizeHeight = counterSizeHeight + myImage.contentHeight
        myImage.alpha = 0
        transition.to( myImage, { alpha=1.0 } )
		scrollView:insert( myImage )
    end
end
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
 
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
	-- Create the widget
	scrollView = widget.newScrollView(
    {
        width = screenWidth,
        height = screenHeight,
		horizontalScrollDisabled = true,
        listener = scrollListener
    })
	
	sceneGroup:insert(scrollView)
	
	-- загружаем ссылки из файла
	linkImage = loadInfo("images.json"); 
	
	if (linkImage) then
		for k,v in pairs(linkImage) do
          for k1,v1 in pairs (v) do
			network.download(v1, "GET", networkListener, string.format("%i.png", counterImage), system.TemporaryDirectory)
			counterImage = counterImage + 1
          end
        end
	end
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene